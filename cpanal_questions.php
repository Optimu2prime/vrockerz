
<?php
  session_start();
	if (!isset($_SESSION['username']))
	header('location:signin.html');
require_once('config.php');
echo "<head>";
echo "<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>";
echo "<link rel='stylesheet' type='text/css' href='style_menu.css' />";
echo "<link rel='stylesheet' type='text/css' href='dist/css/bootstrap.css' />";
echo"<meta charset='UTF-8'>";
echo "<style>";
echo "body { 
	font: 14px/1.4 Georgia, Serif; 
}
#c {
  
	margin:50px;
	 padding-top:25px;
}
p {
	margin: 20px 0; 
}

	/* 
	Generic Styling, for Desktops/Laptops 
	*/
	table { 
	   
	     margin:50px 0;
		width: 100%; 
		border-collapse: collapse; 
	}
	/* Zebra striping */
	tr:nth-of-type(odd) { 
		background: #eee; 
	}
	th { 
		background: #333; 
		color: white; 
		font-weight: bold; 
	}
	td, th { 
		padding: 6px; 
		border: 1px solid #ccc; 
		text-align: left; 
	}

#search input[type='text'] {
    background: url(search-dark.png) no-repeat 10px 6px #fcfcfc;
    border: 1px solid #d1d1d1;
    font: bold 13px Arial,Helvetica,Sans-serif;
    color: #000000;
    width: 350px;
	margin-top:5px;
    padding: 5px 15px 5px 35px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border-radius: 20px;
    text-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
    -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
    -webkit-transition: all 0.7s ease 0s;
    -moz-transition: all 0.7s ease 0s;
    -o-transition: all 0.7s ease 0s;
    transition: all 0.7s ease 0s;
    }

#search input[type='text']:focus {
    width: 400px;
    }
	";
	

echo "</style>";
echo "</head>";

echo "<body>";
echo "<div id='cssmenu'>";
echo "<ul>";
echo  "<li ><a href='cpanal_home.php'><span>All users</span></a></li>
   <li class='active'><a href='cpanal_score.php'><span>Scored users</span></a></li>
    <li><a href='cpanal_recharge.php'><span>Recharge</span></a></li>
    <li><a href='cpanal_questions.php'><span>Questions</span></a></li>
   <li style='margin-left:100px;' class='last'> <form method='post' action='wl_search.php' id='search'>
<input name='q' type='text' size='40' placeholder='Search by name. . .' />
</form></li>
    <li style='float:right';><a href='signout.php'><span>Sign out</span></a></li>
   <li style='float:right';><a href='home.html'><span>Home</span></a></li>;";
echo "</ul>";
echo "</div>";
echo "<div id='c'>";
?>
<form method="POST" class="form-horizontal" action="questionupdate.php">
        <fieldset>
          <legend>Update Number of Questions here</legend>
          <div class="control-group">
            <label class="control-label" for="easy">Easy</label>
            <div class="controls">
              <input type="text" class="input-xlarge" id="easy" name="easy">
              <p class="help-block">Number of Easy Questions</p>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="medium">Medium</label>
            <div class="controls">
              <input type="text" class="input-xlarge" id="medium" name="medium">
              <p class="help-block">Number of Medium Questions</p>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="hard">Hard</label>
            <div class="controls">
              <input type="text" class="input-xlarge" id="hard" name="hard">
              <p class="help-block">Number of Hard Questions</p>
            </div>
          </div>
          <div class="form-actions">
            <button type="submit" class="btn btn-primary">Save changes</button>
        </fieldset>
</form>
<?php
echo "</div>";
echo "</body>";
?> 