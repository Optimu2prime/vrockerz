<?php
	require_once('config.php');
?>
<!DOCTYPE html>

<html>
  <head>
    <title>Vrockerz</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <style>

	body {
        
        padding-top:2%;
		padding-bottom:2%;
		background-color: #dfdfdf;
      }
	   .form-signin {
        
		
        padding: 0px 29px 0px 29px;;
      
		
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
	  .form-signup {
       
       padding: 0px 29px 0px 29px;;
       margin-top:0;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
	   .form-signup .checkbox {
        margin-bottom: 10px;
      }
      .form-signup input[type="text"],
      .form-signup input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    
  
  </head>
  <body style="background-image:url('background.jpg');background-repeat:no-repeat;background-attachment:fixed;background-position:center;background-size:120% 140%;">
     <div id="fb-root"></div>
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '<?php echo $app_id; ?>',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.0' // use version 2.0
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
  //    document.getElementById('status').innerHTML =
 //       'Thanks for logging in, ' + response.name + '!';
 
 	var redirection = "checkuserdb.php?email="+response.email;
		window.location.assign(redirection);
  });
  }
  function redirect()
  {
	  FB.api('/me', function(response) {
 	var redirection = "checkuserdb.php?email="+response.email;
	alert(response.name);
		window.location.assign(redirection);
    });
  }
</script>
<!--
  Below we include the Login Button social plugin. This button uses the JavaScript SDK to
  present a graphical Login button that triggers the FB.login() function when clicked. -->

	   
	 <div class="col-md-10 col-md-offset-0" style="margin-top:0%;">
    <img src="logo.png" style="width:350px;height:150px; margin-left:;"/>
	</div>
      
      <div id="login" class="form-signin col-md-offset-4 col-md-4 col-sm-5 " style="box-shadow:0px 0px 6px #ccc">
          <form method="POST" action="login_script.php">
           <div class="page-header" style="margin-top:0px;"> <h3 class="form-signin-heading" >Sign in</h3></div>
		   <h5 style="margin-top:-10px"> <small >Sign in using social network:</small></h5>
		  <button style='background-color:#fff;border:0px;'>
          
        
        
          
<!--<fb:login-button scope="public_profile,email" onlogin="checkLoginState(0);" onClick="redirect();">
</fb:login-button>
<div onClick="redirect();"><img src="fb_login.png"></div> <br />

<div id="status">
</div>

-->
<div class="fb-login-button" onlogin="checkLoginState(); data-max-rows="1" data-size="medium" data-show-faces="true" data-auto-logout-link="true"></div>

        
        
        </button>
		   <div class="page-header " style="margin-top:15px;">          
              
			</div> 
			<h5 style="margin-top:-10px"> <small >Sign in using our registered account:</small></h5>
            <input type="text" class="form-control" placeholder="Phone number" name="phonenumber">
            <input type="password" class="form-control" placeholder="Password" name="password">
           <p class="text-primary"> 
           
            <button class="btn btn-large btn-primary" style="float:right" type="submit">Sign in</button>
            <br/><br/>
            
          </form>
      
        <br/>   
        </div>
		<h4 class="text-center  col-md-4 col-md-offset-4" style="margin-top:2%;"><small style="padding-right:1%">Not registered yet</small><a href="sign_up.php">Sign up!</a></h4>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="dist/js/bootstrap.min.js"></script>
  
  </body>
</html>
