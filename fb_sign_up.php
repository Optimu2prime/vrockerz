<?php

$signed_request=$_POST['signed_request'];

function parse_signed_request($signed_request) {
  list($encoded_sig, $payload) = explode('.', $signed_request, 2); 

  $secret ="335aa1190d06b774f00ed371e7fc3347"; // Use your app secret here

  // decode the data
  $sig = base64_url_decode($encoded_sig);
  $data = json_decode(base64_url_decode($payload), true);

  // confirm the signature
  $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
  if ($sig !== $expected_sig) {
    error_log('Bad Signed JSON signature!');
    return null;
  }

  return $data;
}

function base64_url_decode($input) {
  return base64_decode(strtr($input, '-_', '+/'));
}

$username=parse_signed_request($signed_request)[registration][name];
$email=parse_signed_request($signed_request)[registration][email];

// 
//
//
//
//
//
//
//
//
?>
<html>
<head>
 <title>Vrockerz</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
 <style>

	body {
        
        padding-top:2%;
		padding-bottom:2%;
		background-color: #dfdfdf;
      }
	   .form-signin {
        
		
        padding: 0px 29px 0px 29px;;
      
		
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
	  .form-signup {
       
       padding: 0px 29px 0px 29px;;
       margin-top:0;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
	   .form-signup .checkbox {
        margin-bottom: 10px;
      }
      .form-signup input[type="text"],
      .form-signup input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>

</head>
 <body style="background-image:url('background.jpg');background-repeat:no-repeat;background-attachment:fixed;background-position:center;background-size:120% 140%;">
     <div class="col-md-10 col-md-offset-0" style="margin-top:0%;">
    <img src="logo.png" style="width:350px;height:150px; margin-left:;"/>
	</div>
 <div id="login" class="form-signup col-md-4 col-md-offset-4 col-sm-5 col-sm-offset-2" style="box-shadow:0px 0px 6px #ccc">
          <form method="POST" action="sign_up_script.php">
             <div class="page-header" style="margin-top:0px;"> <h3 class="form-signin-heading" >Register</h3></div>
             <input type="text" class="form-control"  placeholder="<?php echo $username;?>" value="<?php echo $username;?>" name="username">
			  <input type="text" class="form-control" placeholder="<?php echo $email;?>" value="<?php echo $email;?>" name="email">
		
            <input type="text" class="form-control" placeholder="Phone Number" name="phonenumber">
				<select class="form-control" placeholder="Carrier" name="carrier" style="margin-bottom:15px">
 <option>Airtel</option>
  <option>Vodafone india</option>
  <option>Reliance</option>
  <option>Idea</option>
  <option>Tata Docomo</option>
  <option>Aircel</option>
  <option>Uninor</option>
  <option>MTS India</option>
  <option>Videocon</option>
  <option>MTNL</option>
  <option>Loop Mobile India</option>
  <option>RIL INTOTEL</option>
  <option>Bsnl</option>
</select>
            <input type="password" class="form-control" placeholder="Password" name="password">
		    <input type="password" class="form-control" placeholder="Confirm password" name="confirm_password">
            <p class="text-primary"> <?php 
			session_start();
			if(isset($_SESSION['error1']))
			{echo $_SESSION['error1'];
			  unset($_SESSION['error1']);
			}
			?></p><br/>
             <label style="width:50%">
       <input type="checkbox" > <div class="text-muted" style="margin-top:-21px;margin-left:20px;" ><small> I have read and i agree to <a href="terms.html">terms and conditions</a></small></div>
      </label>
            <button class="btn btn-large btn-primary"  style="float:right" type="submit">Register</button>
			
            <br/><br/>
            
          </form>
      
        <br/>   
        </div>
			<h4 class="text-center  col-md-4 col-md-offset-4" style="margin-top:2%;"><small style="padding-right:1%">Already had an account</small><a href="sign_in.php">Sign in!</a></h4>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="dist/js/bootstrap.min.js"></script>
	<!-------------------------------------->
	<!-------------------------------------->
	<!-------------------------------------->
	<!-------------------------------------->
	<!--------------------------------------><!-------------------------------------->
	<!-------------------------------------->
	<!-------------------------------------->
	<!-------------------------------------->
	<!-------------------------------------->
</body>

</html>